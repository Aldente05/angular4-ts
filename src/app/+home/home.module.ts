import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {homeRouting} from './home.routing';
import {MgsModule} from '../shared/mgs.module';
import {HomeComponent} from './home.component';

@NgModule({
    imports: [
        CommonModule,
        homeRouting,
        MgsModule
    ],
    declarations: [HomeComponent]
})
export class HomeModule {
}
