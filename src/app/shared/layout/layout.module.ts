import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import { MainLayoutComponent } from './app-layouts/main-layout.component';
import {RouterModule} from '@angular/router';
import { AuthLayoutComponent } from './app-layouts/auth-layout.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,

  ],
  declarations: [
    MainLayoutComponent,
    AuthLayoutComponent,
  ]
})
export class MgsLayoutModule {

}
